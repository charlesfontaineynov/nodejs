/*
Todo List :
- Télécharger POSTMAN
- npm install axios
- CRUD utilisateur
- https://jsonplaceholder.typicode.com/
- GET /users/:id/posts
- GET /users/:id/albums
  - Remplir chaque album avec toutes les photos
*/

const express = require('express')
const bodyParser = require('body-parser');
const axios = require('axios');
const app = express();
const PORT = process.env.PORT || 3000;

/* -------------------------------------------------------------------------- */
// Build CRUD a user : //Create //Read or Retrieve //Update //Delete or Destroy
/* -------------------------------------------------------------------------- */

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// GET Home (http://localhost:3000)

app.get('/', function (req, res, next) {
  res.send('Page d\'accueil !');
  next();
});

// GET ALL users

app.get('/users', async function (req, res) {
    try {
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');

        // res.send('Affiche tous les utilisateurs');
        console.log(res.send(response.data));

    } catch (error) {
        console.error(error);
    }
});

/** Start CRUD **/

// GET user id

app.get('/users/:id', async function (req, res) {
    try{
        const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}`)
        res.send(response.data);
    } catch(error){
        res.send(error)
    }
});

// POST new user

app.post('/users/:id', async function (req, res) {
    req.query = {
        "id" : req.params.id,
        "name" : "Charles Fontaine",
        "username" : "Charly",
        "email" : "charles.fontaine@ynov.com",
        "address": {
            "street": "Quai des Chartrons",
            "suite": "89",
            "city": "Bordeaux",
            "zipcode": "33300",
            "geo": {
                "lat": "44.853897",
                "lng": "-0.565962"
            }
        },
        "phone": "0 800 60 06 33",
        "website": "https://ynov-bordeaux.com/",
        "company": {
            "name": "Ynov Campus",
            "catchPhrase": "Multi-layered client-server neural-net",
            "bs": "harness real-time e-markets"
        }
    }
    try{
        const response = await axios.post('https://jsonplaceholder.typicode.com/users', req.query);
        res.send(response.data);
    } catch(error){
        res.send(error);
    }
});

// UPDATE user

app.put('/users/:id', async function(req, res){
    try {
        req.query = {'name': 'Will Smith', 'username': 'Prince of Bel Air', 'job': 'Actor'};

        const response = await axios.put(`https://jsonplaceholder.typicode.com/users/${req.params.id}`, req.query);
        res.send(response.data);
    } catch(error) {
        res.send(error);
    }
});

// DELETE user

app.delete('/users/:id', async function (req, res) {
    try{
        const response = await axios.delete(`https://jsonplaceholder.typicode.com/users/${req.params.id}`, );
        res.send(response.data);

    } catch(error){
        res.send(error)
    }
});

/** End CRUD **/

// GET all posts from id

app.get('/users/:id/posts', async function(req,res){
    try{
        const response = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}/posts`);
        res.send(response.data);

    } catch(error){
        res.send(error);
    }
});

// GET all albums and add all pics for each user's albums

app.get('/users/:id/albums', async function(req,res){
    try{
        const users = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}`);
        const albums = await axios.get(`https://jsonplaceholder.typicode.com/users/${req.params.id}/albums`);
        const pics = await axios.get('https://jsonplaceholder.typicode.com/photos');

        for (album of albums.data) {
            album['pics'] = pics.data;
        }

        res.send(albums.data);

    } catch(error){
        res.send(error);
    }
})


// LISTEN for requests on PORT

app.listen(PORT, (error) => {
    if (error) console.log(error);
    console.log('Server is running on port', PORT);
});

